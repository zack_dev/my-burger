import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://zreact-myburger.firebaseio.com/'
})

export default instance
